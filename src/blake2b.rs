/**
 * @file sha512.h
 * @brief SHA-512 (Secure Hash Algorithm 512)
 *
 * @section License
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * Copyright (C) 2010-2021 Oryx Embedded SARL. All rights reserved.
 *
 * This file is part of CycloneCRYPTO Open.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @author Oryx Embedded SARL (www.oryx-embedded.com)
 * @version 2.0.2
 *
 * Ported from C to Rust 2020 by microengineer18@gmail.com
 **/

use digest::consts::{U128, U32};
use digest::{BlockInput, FixedOutputDirty, Output, Reset, Update};

type BlockSize = U128;

macro_rules! ROR64 {
    ($a:expr,$n:expr) => {
        ((($a) >> ($n)) | (($a) << (64 - ($n))))
    };
}


macro_rules! G {
    ($a:expr, $b: expr, $c:expr, $d:expr, $x:expr, $y:expr) => {
        $a = $a.wrapping_add($b.wrapping_add($x)); 
        $d ^= $a; 
        $d = ROR64!($d, 32); 
        $c = $c.wrapping_add($d); 
        $b ^= $c; 
        $b = ROR64!($b, 24); 
        $a = $a.wrapping_add($b.wrapping_add($y)); 
        $d ^= $a; 
        $d = ROR64!($d, 16); 
        $c = $c.wrapping_add($d); 
        $b ^= $c; 
        $b = ROR64!($b, 63);
    };
}

const SIGMA: [[u8;16];12]=[
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    [14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3],
    [11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4],
    [7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8],
    [9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13],
    [2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9],
    [12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11],
    [13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10],
    [6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5],
    [10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
    [14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3],
];

const IV: [u64;8] = [
    0x6A09E667F3BCC908, 0xBB67AE8584CAA73B, 0x3C6EF372FE94F82B, 0xA54FF53A5F1D36F1,
    0x510E527FADE682D1, 0x9B05688C2B3E6C1F, 0x1F83D9ABFB41BD6B, 0x5BE0CD19137E2179,
];


#[derive(Clone)]
pub struct Blake2b {
    h: [u64;8],
    buffer: [u8;128],
    size: usize,
    total_size: [u64;2],
    digest_size: usize,
}

impl Blake2b {
    pub fn new() -> Self {
        let mut h = Self{
            h: [0u64;8],
            buffer: [0u8;128],
            size: 0,
            total_size: [0u64;2],
            digest_size: 0,
        };
        h.init(&[], 32);
        h
    }

    pub fn init(&mut self, key: &[u8], digest_len: usize) -> bool {
        if key.len() > 64 {
            return false;
        }
        if digest_len < 1 || digest_len > 64 {
            return false;
        }
        for i in 0..8 {
            self.h[i] = IV[i];
        }
        self.h[0] ^= digest_len as u64;
        self.h[0] ^= (key.len() as u64) << 8;
        self.h[0] ^= 0x01010000;

        self.size = 0;

        self.total_size[0] = 0;
        self.total_size[1] = 0;

        self.digest_size = digest_len;
        self.buffer = [0u8;128];

        if key.len() > 0 {
            self.buffer[0..key.len()].clone_from_slice(&key);
        }
        true
    }

    pub fn update(&mut self, data: &[u8]) {
        let mut length = data.len();
        let mut data_ofs = 0;

        while length > 0 {
            if self.size == 128 {
                self.process_block(false);
                self.size = 0;
            }
            let n = if length < 128 - self.size { length } else { 128 - self.size };

            self.buffer[self.size..self.size+n].clone_from_slice(&data[data_ofs..data_ofs+n]);
            self.size += n;
            data_ofs += n;
            length -= n;
        }
    }

    pub fn digest(&mut self, digest: &mut [u8]) {
        for i in self.size..128 {
            self.buffer[i] = 0;
        }
        self.process_block(true);

        for i in 0..4 {
            digest[i*8..(i+1)*8].clone_from_slice(&self.h[i].to_le_bytes());
        }
    }

    pub fn process_block(&mut self, last: bool) {
        let mut v = [0u64;16];
        let mut m = [0u64;16];

        for i in 0..8 {
            v[i] = self.h[i];
            v[i+8] = IV[i];
        }
        self.total_size[0] += self.size as u64;

        if self.total_size[0] < self.size as u64 {
            self.total_size[1] += 1;
        }

        v[12] ^= self.total_size[0];
        v[13] ^= self.total_size[1];

        if last {
            v[14] = !v[14];
        }

        for i in 0..16 {
            let mut bytes = [0u8;8];
            bytes.clone_from_slice(&self.buffer[i*8..(i+1)*8]);
            m[i] = u64::from_le_bytes(bytes);
        }

        for i in 0..12 {
            //The column rounds apply the quarter-round function to the four
            //columns, from left to right
            G!(v[0], v[4], v[8],  v[12], m[SIGMA[i][0] as usize], m[SIGMA[i][1] as usize]);
            G!(v[1], v[5], v[9],  v[13], m[SIGMA[i][2] as usize], m[SIGMA[i][3] as usize]);
            G!(v[2], v[6], v[10], v[14], m[SIGMA[i][4] as usize], m[SIGMA[i][5] as usize]);
            G!(v[3], v[7], v[11], v[15], m[SIGMA[i][6] as usize], m[SIGMA[i][7] as usize]);

            //The diagonal rounds apply the quarter-round function to the top-left,
            //bottom-right diagonal, followed by the pattern shifted one place to
            //the right, for three more quarter-rounds
            G!(v[0], v[5], v[10], v[15], m[SIGMA[i][8] as usize],  m[SIGMA[i][9] as usize]);
            G!(v[1], v[6], v[11], v[12], m[SIGMA[i][10] as usize], m[SIGMA[i][11] as usize]);
            G!(v[2], v[7], v[8],  v[13], m[SIGMA[i][12] as usize], m[SIGMA[i][13] as usize]);
            G!(v[3], v[4], v[9],  v[14], m[SIGMA[i][14] as usize], m[SIGMA[i][15] as usize]);            
        }
        for i in 0..8 {
            self.h[i] ^= v[i] ^ v[i+8];
        }
    }
}

impl Default for Blake2b {
    fn default() -> Self {
        Blake2b::new()
    }
}

impl BlockInput for Blake2b {
    type BlockSize = BlockSize;
}

impl Update for Blake2b {
    fn update(&mut self, input: impl AsRef<[u8]>) {
        self.update(input.as_ref());
    }
}

impl FixedOutputDirty for Blake2b {
    type OutputSize = U32;
    fn finalize_into_dirty(&mut self, out: &mut Output<Self>) {
        let mut digest = [0u8;32];
        self.digest(&mut digest);
        out.copy_from_slice(&digest);
    }
}

impl Reset for Blake2b {
    fn reset(&mut self) {
        self.init(&[], 32);
    }
}

